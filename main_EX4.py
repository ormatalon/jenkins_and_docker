
#FILE_PATH = r"C:\Users\user\Desktop\ITC_bootcamp\Frameworks_and_tools\Jenkins_and_Dockers\jenkins_and_docker/url_list.txt"

import os
txt_input = os.environ["FILE_PATH"]

try:
    f = open(txt_input,'r')
    url_list = f.read().split(",")
except FileNotFoundError:
    print("file not found")
except NameError:
    print("name error")
finally:
    f.close()

print(url_list)

"""import requests

for url in url_list:
    req = requests.get(url)
    print(req.text)"""
